<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([ 'register' => false ]);

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/payment', 'PesertaController@payment')->name('payment')->middleware('auth');
Route::get('/history', 'PesertaController@history')->name('history.payment')->middleware('auth');
Route::get('/peserta','PesertaController@index')->middleware('auth');
Route::get('/peserta/json','PesertaController@json')->name('users.index')->middleware('auth');
Route::get('/cek/merchant', 'PesertaController@merchant')->name('get.merchant')->middleware('auth');
Route::get('/cek/connection', 'PesertaController@connection')->name('get.connection')->middleware('auth');
Route::get('/send', 'PesertaController@sendEmail')->middleware('auth');

Route::post('/sakti/cek/payment', 'PesertaController@payment')->name('cek.payment')->middleware('auth');
Route::post('/sakti/cek/history', 'PesertaController@history')->name('cek.history')->middleware('auth');

