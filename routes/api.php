<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/sakti', 'SaktiController@index');
// Route::get('/sakti/json', 'SaktiController@testJson');
// Route::get('/sakti/qrcode', 'SaktiController@testQr');
// Route::get('/sakti/check/merchant', 'SaktiController@checkConfigMerchant')->middleware('api:auth');
// Route::get('/sakti/check/connection', 'SaktiController@checkConnectionSakti')->middleware('api:auth');

Route::post('/sakti/create/qrcode', 'SaktiController@createQrCode');
Route::post('/sakti/create/wbt', 'SaktiController@createWbt');
Route::post('/sakti/cek/payment', 'SaktiController@cekPaymentStatus');
Route::post('/sakti/cek/history/transaction','SaktiController@cekHistoryTransaction');
