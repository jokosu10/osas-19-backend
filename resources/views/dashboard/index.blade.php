@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <table class="table table-bordered table-datatable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Country</th>
                    <th>Tshirt</th>
                    <th>Transaction No</th>
                    <th>Amount Tiket</th>
                    <th>Image KTM</th>
                    <th>Url QRCode</th>
                    <th>Timestap without timezone</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td class="non_searchable"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
@endsection

@section('javascript')
    <script>
      $('.table-datatable').DataTable({
        processing: true,
        serverSide: true,
        lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        ajax: '{{ route('users.index') }}',
        columns: [
          {data: 'id', name: 'id'},
          {data: 'name', name: 'name'},
          {data: 'email', name: 'email'},
          {data: 'phone_number', name: 'phone_number'},
          {data: 'country', name: 'country'},
          {data: 'size_tshirt', name: 'size_tshirt'},
          {data: 'transaction_no', name: 'transaction_no'},
          {data: 'amount', name: 'amount'},
          {
            data: 'image',
            name: 'image',
            render: function( data, type, full, meta ) {
              if (data) {
                return "<img src=\"{!! url('/images/') !!}/" + data + "\" height=\"50\"/>";
              } else {
                return "No Image";
              }
            },
            orderable: true,
            searchable: true
          },
          {data: 'url_qrcode', name: 'url_qrcode'},
          {data: 'created_at', name: 'created_at'},
        ],
        dom: 'Blfrtip',
        buttons: [
          'copyHtml5',
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
        ],
        initComplete: function () {
          this.api().columns().every(function () {
            var column = this;
            //example for removing search field
            if (column.footer().className !== 'non_searchable') {
              var input = document.createElement("input");
              $(input).appendTo($(column.footer()).empty())
              .keyup(function () {
                column.search($(this).val(), false, false, true).draw();
              });
            }
          });
        }
      });
    </script>
@endsection
