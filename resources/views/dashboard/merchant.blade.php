@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h1>Cek Config Merchant</h1>
    </div>
    <br/>
    <div class="row justify-content-center">
        <table class="table table-bordered table-datatable">
            <thead>
                <tr>
                    <th>Admin</th>
                    <th>Callback</th>
                    <th>Fee</th>
                    <th>Limit Saldo</th>
                    <th>Limit Transaction</th>
                    <th>Max Transaction</th>
                    <th>Merchant Name</th>
                    <th>Min Transaction</th>
                    <th>Merchant Name</th>
                    <th>Merchant Saldo</th>
                </tr>
            </thead>
            <tbody>
                @if($data)
                    <tr>
                        <td>{{ $data->data->admin }}</td>
                        <td>{{ $data->data->callback }}</td>
                        <td>{{ $data->data->fee }}</td>
                        <td>{{ $data->data->limit_saldo }}</td>
                        <td>{{ $data->data->limit_transaction }}</td>
                        <td>{{ $data->data->max_transaction }}</td>
                        <td>{{ $data->data->merchant_name }}</td>
                        <td>{{ $data->data->min_transaction }}</td>
                        <td>{{ $data->data->name }}</td>
                        <td>{{ $data->data->saldo }}</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('javascript')
    <script>
        $(".table-datatable").DataTable();
    </script>
@endsection
